Auxilary repo to the project https://github.com/fernandopassaia/app_fullstackdemo

0. create pv with 

   $ kubectl apply -f pv.yaml


1. Install postgresql with helm:

   $ helm repo add bitnami https://charts.bitnami.com/bitnami
   $ helm upgrade --install postgresql bitnami/postgresql \
      --set auth.postgresPassword="@1234Fd#"
      --set global.storageClass="manual"


2. apply manifest:

   $ kubectl apply -f deploy.yaml
   
3. go to domain name